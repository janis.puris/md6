Vidzemes Augstskola
Inženierzinātņu fakultāte
Programmēšana II

# MD6

1.	Izveidojiet .NET ietvara programmatūras lietotni izmantojot Visual Studio 2010 un grafisko elementu attēlošanas rīku;  
2.	Papildiniet mājas darbu un turpiniet uzlabot mājas darba lietotnes funkcionalitāti;  
3.	Nodemonstrēt datu bāzes pielietojumu;  
4.	Mājas darbam ir jānodrošina galvenās tās funkcionalitātes