﻿using System;

namespace Calculator
{
    // Inheriting from Calculation class
    public class MonthlyPaymentCalculation : Calculation, ICalculatable
    {
        public new double MonthlyPayment { get; private set; }

        public void Calculate()
        {
            // Local helper vars
            double _monthlyInterestRate;
            int _numMonthlyPayments;

            // Calculate the helper vars with helper methods
            _numMonthlyPayments = NumberOfMonthlyPayments(IsLoanTermMonthly, LoanTerm);
            _monthlyInterestRate = CalculateMonthlyRate(InterestRate);

            // Calculate Mothly payment
            MonthlyPayment = Math.Round(CalculateMonthlyPayment(_monthlyInterestRate, LoanAmount, _numMonthlyPayments),2);
        }

        public double Result()
        {
            return MonthlyPayment;
        }
    }
}
