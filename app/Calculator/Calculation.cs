﻿using System;

namespace Calculator
{
    // Interfacing from ICalculatable
    public abstract class Calculation
    {
        // Implementing the properties from Interface
        public double LoanAmount { get; set; }
        public int LoanTerm { get; set; }
        public double InterestRate { get; set; }
        public double MonthlyPayment { get; set; }
        public bool IsLoanTermMonthly { get; set; }

        // Calculation class method, this method will be inherited as is
        public int NumberOfMonthlyPayments(bool isLoanTermMonthly, int loanTermValue)
        {
            if (isLoanTermMonthly)
            {
                return loanTermValue;
            }
            else
            {
                return loanTermValue * 12;
            }
        }

        // Calculation methods
        // Monthly Payment
        protected double CalculateMonthlyPayment(double i, double a, int n)
        {
            return (a * i) / (1 - Math.Pow((1 + i), n * -1));
        }
        // Interest Rate
        protected double CalculateInterestRate(double a, double p, int n)
        {
            double q = Math.Log(1 + (1.0 / n)) / Math.Log(2);
            double i = Math.Pow((Math.Pow(1 + (p / a), 1.0 / q) - 1), q) - 1;
            return i * 12 * 100;
        }
        // Loan Term
        protected int CalculateLoanTerm(double i, double p, double a)
        {
            return (int)Math.Ceiling((Math.Log(1 - ((i * a) / p)) * -1) / Math.Log(1 + i));
        }
        // Loan Amount
        protected double CalculateLoanAmount(double i, double p, int n)
        {
            return (p / i) * (1 - Math.Pow((1 + i), n * -1));
        }
        // Calculation methods end

        protected double CalculateMonthlyRate(double i)
        {
            return Math.Pow((1 + i / 100.0), (1.0 / 12.0)) - 1;
        }
    }
}
