﻿using System;

namespace Calculator
{
    // Inheriting from Calculation class
    public class InterestRateCalculation : Calculation, ICalculatable
    {
        public new double InterestRate { get; private set; }

        public void Calculate()
        {
            // Local helper vars
            double _monthlyInterestRate;
            int _numMonthlyPayments;

            // Calculate the helper vars with helper methods
            _numMonthlyPayments = NumberOfMonthlyPayments(IsLoanTermMonthly, LoanTerm);
            _monthlyInterestRate = CalculateMonthlyRate(InterestRate);

            // Calculate Mothly payment
            InterestRate = Math.Round(CalculateInterestRate(LoanAmount, MonthlyPayment, _numMonthlyPayments), 2);
        }

        public double Result()
        {
            return InterestRate;
        }
    }
}
